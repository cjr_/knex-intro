const express = require('express');

const router = express.Router();

const queries = require('../db/queries'); // require the queries file

router.get('/icecream', (req, res) => {
  queries
    .icecream
    .getAll()
    .then(icecreams => {
      res.json(icecreams);
    });
});

router.get('/icecream/:id', (req, res) => {
  queries
    .icecream
    .getOne(req.params.id)
    .then(icecream => {
      res.json(icecream);
    });
});

router.post('/icecream', (req, res) => {
  queries
    .icecream
    .create(req.body)
    .then(results => {
      res.send(results[0]);
    });
});

module.exports = router;
