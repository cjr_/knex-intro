
exports.up = function(knex, Promise) {
  return knex.schema
  .createTable('icecream', function(table){
    table.increments('id').primary();
    table.string('flavor').notNullable();
    table.string('color');
    table.integer('rating');
  })
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTableIfExists('icecream');
};
