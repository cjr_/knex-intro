const knex = require('./knex');

module.exports = {
  icecream: {
    getAll: function() {
      return knex('icecream');
    },
    getOne: function(id) {
      return knex('icecream').where('id', id).first();
    },
    create: function(icecream) {
      return knex('icecream').insert(icecream).returning('*');
    }
  }
}
