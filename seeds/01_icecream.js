
exports.seed = function(knex, Promise) {
  return knex('icecream').del()
    .then(function () {
      const icecreams = [{
        flavor: 'pistachio',
        color: 'green',
        rating: 7
      }, {
        flavor: 'mint chocolate chip',
        color: 'green',
        rating: 10
      }, {
        flavor: 'cherry garcia',
        color: 'pink',
        rating: 6
      }, {
        flavor: 'vanilla',
        color: 'white',
        rating: 2
      }, {
        flavor: 'rocky road',
        color: 'brown',
        rating: 5
      }]

      return knex('icecream').insert(icecreams);
    });
};
